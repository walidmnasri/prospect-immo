<?php

namespace App\Repository;

use App\Entity\Properties;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Property|null find($id, $lockMode = null, $lockVersion = null)
 * @method Property|null findOneBy(array $criteria, array $orderBy = null)
 * @method Property[]    findAll()
 * @method Property[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PropertiesRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Properties::class);
    }

     /**
      * @param string $key
      * @param mixed $val
      * @param int limit
      * @param string $orderBy
      * @return Properties[] Returns an array of Properties objects
     */

    public function findByField($key, $val, $limit, $orderBy)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.' . $key. ' = :val')
            ->andWhere('a.isPublish= :val2')
            ->setParameter('val', $val)
            ->setParameter('val2', 1)
            ->orderBy('a.' . $orderBy, 'ASC')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @param mixed $key
     * @param mixed $val
     * @return Properties
     * @throws \Doctrine\ORM\NonUniqueResultException
     */

    public function findOneByField($key, $val): ?Properties
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.' . $key. ' = :val')
            ->setParameter('val', $val)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
}
