<?php
// src/Controller/ManageController.php
namespace App\Controller;

use App\Entity\Categories;
use App\Entity\Pictures;
use App\Entity\Properties;
use App\Entity\Regions;
use App\Entity\Users;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ManageController extends AbstractController
{
    /**
     * @var array images
     */
    protected $images = [];

    /**
     * @Route("/new", name="new")
     */
    public function addProperty(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $property = new Properties();
        $categoriesRepo = $this->getDoctrine()->getRepository(Categories::class);
        $regionRepo = $this->getDoctrine()->getRepository(Regions::class);
        $userRepo = $this->getDoctrine()->getRepository(Users::class);
        $categories = $categoriesRepo->findAll();
        $regions = $regionRepo->findAll();
        $success = false;

        if (!empty($request->request->all())) {
            $post = $request->request->all();

            $admin = $userRepo->find(1);
            foreach ($post['property'] as $key => $val) {
                if ($key == "category") {
                    $categoryObj = $categoriesRepo->find($val);
                    $property->setCategory($categoryObj);
                } elseif ($key == "region") {
                    $regionObj = $regionRepo->find($val);
                    $property->setRegion($regionObj);
                } else {
                    $property->{'set' . ucfirst ($key)}($val);
                }
            }
            $property->setCreatedAt(new \DateTime());
            $property->setProposedBy($admin);
            $em->persist($property);
            $em->flush();

            $images = $this->get('session')->get('images');
            if (isset($images) && !empty($images)) {
                foreach ($images as $key => $elem) {
                    if ($key == 0) {
                        $property->setPrincipalPicture($elem);
                        $em->persist($property);
                    }
                    $picture = new Pictures();
                    $picture->setName($elem);
                    $picture->setDefaultDir('/uploads/images/');
                    $picture->setProperty($property);
                    $em->persist($picture);
                    $em->flush();
                }

                $this->get('session')->set('images', null);
            }
            
            $success = true;
        }

        return $this->render('new.html.twig', [
            'success' => $success,
            'categories' => $categories,
            'regions' => $regions
        ]);
    }

    /**
     * @Route("/upload", name="upload")
     */
    public function upload()
    {
        if(isset($_FILES['file'])) {
            if(move_uploaded_file($_FILES["file"]["tmp_name"], __DIR__ . "/../../public/uploads/images/".$_POST["filename"])) {
                $sessionVal = $this->get('session')->get('images');
                $sessionVal[] = $_POST["filename"];
                // Set value back to session
                $this->get('session')->set('images', $sessionVal);
                echo 'Upload Success';
            } else {
                echo '#Fail';
            }
        } else {
            header("Location: http://www.inwebson.com/html5/html5-drag-and-drop-file-upload-with-canvas");
        }

        exit;
    }

}
