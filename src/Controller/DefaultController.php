<?php
// src/Controller/IndexController.php
namespace App\Controller;

use App\Entity\Categories;
use App\Entity\Contacts;
use App\Entity\Newsletter;
use App\Entity\Pictures;
use App\Entity\Properties;
use App\Entity\Regions;
use App\Entity\Users;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    const FOR_SELL = 1;
    const FOR_LOCATION = 2;
    const FOR_BAY = 3;

    /**
     * @Route("/", name="index")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        $propertiesRepo = $this->getDoctrine()->getRepository(Properties::class);
        $propertiesToSell = $propertiesRepo->findByField('type', self::FOR_SELL, 10, 'id');
        $propertiesToLocation = $propertiesRepo->findByField('type', self::FOR_LOCATION, 10, 'id');
        $propertiesTrend = $propertiesRepo->findByField('isPublish', 1, 10, 'id');

        return $this->render('index.html.twig', [
           'propertiesToSell' => $propertiesToSell,
           'propertiesToLocation' => $propertiesToLocation,
           'propertiesTrend' => $propertiesTrend,
        ]);
    }

    /**
     * * @Route("/property/{id}", methods={"GET","HEAD"})
     */
    public function property($id)
    {
        $propertiesRepo = $this->getDoctrine()->getRepository(Properties::class);
        $picturesRepo = $this->getDoctrine()->getRepository(Pictures::class);
        $property = $propertiesRepo->find($id);
        $pictures = $picturesRepo->findBy(['property' => $property]);

        return $this->render('property.html.twig', [
            'property' => $property,
            'pictures' => $pictures,
        ]);
    }

    /**
     * @Route("/list", name="list")
     */
    public function listSearch(Request $request)
    {
        $request->getContent();
    }

    /**
     * Matches /a-propos exactly
     *
     * @Route("/a-propos", name="about_us")
     */
    public function aPropos()
    {
        return $this->render('a-propos.html.twig');
    }

    /**
     * @Route("/contact", name="contact")
     * @param Request $request
     * @param \Swift_Mailer $mailer
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function contact(Request $request, \Swift_Mailer $mailer)
    {
        $success = false;
        $em = $this->getDoctrine()->getManager();
        $user = new Users();
        $contact = new Contacts();

        if ($request->getContent()) {
            $post = $request->request->all();
            foreach ($post['user'] as $key => $val) {
                $user->{'set' . ucfirst ($key)}($val);
            }
            $user->setType(3);
            $em->persist($user);
            $em->flush();

            $contact->setMessage($post['message']);
            $contact->setUser($user);
            $em->persist($contact);
            $em->flush();
            $success = true;

            $message = (new \Swift_Message('Un nouveau contact'))
                ->setFrom('mnasri.walid@gmail.com')
                ->setTo('wmnasri@smart-team.tn')
                ->setBody(
                    $this->renderView(
                    // templates/emails/registration.html.twig
                        'emails/new-contact.html.twig',
                        ['user' => $user]
                    ),
                    'text/html'
                )
            ;

            $mailer->send($message);

        }
        return $this->render('contact.html.twig', [
            'success' => $success,
        ]);
    }

    /**
     * @Route("/vente-residentiel", name="sale")
     */
    public function venteResidentiel()
    {
        $propertiesRepo = $this->getDoctrine()->getRepository(Properties::class);
        $propertiesToSell = $propertiesRepo->findByField('type', self::FOR_SELL, 100, 'id');

        return $this->render('vente-residentiel.html.twig', [
            'propertiesToSell' => $propertiesToSell,
            ]);
    }

    /**
     * @Route("/location-residentiels", name="location")
     */
    public function locationResidentiels()
    {
        $propertiesRepo = $this->getDoctrine()->getRepository(Properties::class);
        $propertiesToLocation = $propertiesRepo->findByField('type', self::FOR_LOCATION, 100, 'id');

        return $this->render('location-residentiels.html.twig', [
            'propertiesToLocation' => $propertiesToLocation,
        ]);
    }

    /**
     * @Route("/projets-neufs", name="new-project")
     */
    public function projetsNeufs()
    {
        $propertiesRepo = $this->getDoctrine()->getRepository(Properties::class);
        $propertiesNew = $propertiesRepo->findByField('isNew', 1, 100, 'id');

        return $this->render('projets-neufs.html.twig', [
            'propertiesNew' => $propertiesNew,
        ]);
    }

    /**
     * @Route("/professionnel", name="pro")
     */
    public function professionnel()
    {
        $propertiesRepo = $this->getDoctrine()->getRepository(Properties::class);
        $propertiesPro = $propertiesRepo->findByField('pro', true, 100, 'id');

        return $this->render('professionnel.html.twig', [
            'propertiesPro' => $propertiesPro,
        ]);
    }

    /**
     * @Route("/suggest", name="suggest")
     */
    public function suggest(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $property = new Properties();
        $user = new Users();
        $categoriesRepo = $this->getDoctrine()->getRepository(Categories::class);
        $regionRepo = $this->getDoctrine()->getRepository(Regions::class);
        $categories = $categoriesRepo->findAll();
        $regions = $regionRepo->findAll();
        $success = false;

        if ($request->getContent()) {
            $post = $request->request->all();

            foreach ($post['user'] as $key => $val) {
                $user->{'set' . ucfirst ($key)}($val);
            }
            $user->setType(0);
            $em->persist($user);
            $em->flush();

            foreach ($post['property'] as $key => $val) {
                if ($key == "category") {
                    $categoryObj = $categoriesRepo->find($val);
                    $property->setCategory($categoryObj);
                } elseif ($key == "region") {
                    $regionObj = $regionRepo->find($val);
                    $property->setRegion($regionObj);
                } else {
                    $property->{'set' . ucfirst ($key)}($val);
                }
            }
            $property->setCreatedAt(new \DateTime());
            $property->setProposedBy($user);
            $em->persist($property);
            $em->flush();

            $success = true;
        }

        return $this->render('suggest.html.twig', [
            'success' => $success,
            'categories' => $categories,
            'regions' => $regions
        ]);
    }

    /**
     * @Route("/request", name="request")
     */
    public function request(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $req = new \App\Entity\Request();
        $user = new Users();
        $categoriesRepo = $this->getDoctrine()->getRepository(Categories::class);
        $regionRepo = $this->getDoctrine()->getRepository(Regions::class);
        $categories = $categoriesRepo->findAll();
        $regions = $regionRepo->findAll();
        $success = false;

        if ($request->getContent()) {
            $post = $request->request->all();

            foreach ($post['user'] as $key => $val) {
                $user->{'set' . ucfirst ($key)}($val);
            }
            $user->setType(2);
            $em->persist($user);
            $em->flush();

            foreach ($post['request'] as $key => $val) {
                if ($key == "category") {
                    $categoryObj = $categoriesRepo->find($val);
                    $req->setCategory($categoryObj);
                } elseif ($key == "region") {
                    $regionObj = $regionRepo->find($val);
                    $req->setRegion($regionObj);
                } elseif ($key == "surface") {
                    $req->setSurfaceMax($val + 10);
                    $req->setSurfaceMin($val - 10);
                } else {
                    $req->{'set' . ucfirst ($key)}($val);
                }
            }
            $req->setCreatedAt(new \DateTime());
            $req->setUser($user);
            $em->persist($req);
            $em->flush();

            $success = true;
        }

        return $this->render('request.html.twig', [
            'success' => $success,
            'categories' => $categories,
            'regions' => $regions
        ]);
    }

    /**
     * @Route("/newsletter", name="newsletter")
     */
    public function newsletter(Request $request)
    {
        $newsletter = $request->request->get('newsletter');

        if (filter_var($newsletter, FILTER_VALIDATE_EMAIL)) {
            $em = $this->getDoctrine()->getManager();
            $newsletterObj = new Newsletter();
            $newsletterObj->setEmail($newsletter);
            $em->persist($newsletterObj);
            $em->flush();

            return new JsonResponse(['msg' => 'Votre email a bien été enregistré !', 'status' => 'OK', 'code' => 200]);
        }
        return new JsonResponse(['msg' => "L'adresse email $newsletter n'est valide.", 'status' => 'KO', 'code' => 500]);
    }
}
