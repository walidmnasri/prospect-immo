-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Lun 18 Février 2019 à 00:35
-- Version du serveur :  5.7.25-0ubuntu0.18.10.2
-- Version de PHP :  7.2.10-0ubuntu1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `prospectqguser`
--

-- --------------------------------------------------------

--
-- Structure de la table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `categories`
--

INSERT INTO `categories` (`id`, `name`) VALUES
(1, 'Appartement'),
(2, 'Villa'),
(3, 'Etage de villa'),
(4, 'Duplex / Triplex'),
(5, 'Penthouse'),
(6, 'Terrain'),
(7, 'Bureau'),
(8, 'Local commercial'),
(9, 'Immeuble'),
(10, 'Usine');

-- --------------------------------------------------------

--
-- Structure de la table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(11) NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `contacts`
--

INSERT INTO `contacts` (`id`, `message`, `user_id`) VALUES
(1, 'sdsqd', 9),
(2, 'jkhsdfhjdsfhfds', 10),
(3, 'sqdqsdqdqd', 11),
(4, 'fgfdgdfg', 12),
(5, 'fgfdgdfg', 13),
(6, 'dfgtdgfd', 14),
(7, 'sqdqsdqs', 15),
(8, 'sqdqsdq', 16);

-- --------------------------------------------------------

--
-- Structure de la table `newsletter`
--

CREATE TABLE `newsletter` (
  `id` int(11) NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `newsletter`
--

INSERT INTO `newsletter` (`id`, `email`) VALUES
(1, 'test'),
(2, ''),
(3, 'sqdqsd@sqdqsdqsd.fr'),
(4, 'sqdqsd@sqdqsd.fr');

-- --------------------------------------------------------

--
-- Structure de la table `pictures`
--

CREATE TABLE `pictures` (
  `id` int(11) NOT NULL,
  `property_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `default_dir` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `pictures`
--

INSERT INTO `pictures` (`id`, `property_id`, `name`, `default_dir`) VALUES
(2, 6, '2- Formulaire Inscription de TLS.jpg', '/uploads/images/'),
(3, 6, '46445057_342602936544908_655749430590308352_n.jpg', '/uploads/images/'),
(4, 5, 'Order   OVH.png', '/uploads/images/'),
(5, 6, '2- Formulaire Inscription de TLS.jpg', '/uploads/images/'),
(6, 7, '46445057_342602936544908_655749430590308352_n.jpg', '/uploads/images/'),
(7, 7, '46436434_341675346384368_6834220410288996352_n.jpg', '/uploads/images/'),
(8, 9, '49464861_245906136325922_5163663047666958336_n.jpg', '/uploads/images/');

-- --------------------------------------------------------

--
-- Structure de la table `properties`
--

CREATE TABLE `properties` (
  `id` int(11) NOT NULL,
  `Category_id` int(11) DEFAULT NULL,
  `region_id` int(11) DEFAULT NULL,
  `proposed_by` int(11) DEFAULT NULL,
  `titre` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `sous_titre` text COLLATE utf8mb4_unicode_ci,
  `surface` int(11) DEFAULT NULL,
  `type` int(1) NOT NULL,
  `price` double DEFAULT NULL,
  `pieces` int(11) DEFAULT NULL,
  `principal_picture` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_new` tinyint(1) DEFAULT NULL,
  `closed` tinyint(1) DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_publish` tinyint(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bathroom` int(11) DEFAULT NULL,
  `garage` int(11) DEFAULT NULL,
  `pro` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `properties`
--

INSERT INTO `properties` (`id`, `Category_id`, `region_id`, `proposed_by`, `titre`, `sous_titre`, `surface`, `type`, `price`, `pieces`, `principal_picture`, `is_new`, `closed`, `description`, `is_publish`, `created_at`, `updated_at`, `address`, `bathroom`, `garage`, `pro`) VALUES
(5, 2, 2, 1, 'rtytrytr', NULL, 200, 2, 200000, 5, '20181117_000938.jpg', 1, NULL, 'fghfthgergrgsdfg', 1, '2019-02-10 23:41:44', NULL, 'Appt3 6a Street E Francois, Rose Hill', 2, 1, 1),
(6, 10, 1, 1, 'fgjgfghg', NULL, 120, 2, 600, 0, '20181117_000938.jpg', 1, NULL, 'fgsfgs', 1, '2019-02-10 23:47:31', NULL, '31 Rés. el habib Av du Lac mälaren', 0, 0, NULL),
(7, 3, 2, 1, 'hedha 1 ', NULL, NULL, 1, 300, 3, '20181117_000938.jpg', 1, NULL, 'qqdqsd\r\nqsdqsdqsd\r\nqsdqsdqsd\r\nqsd\r\nqsd\r\nsqdqsdddddddddddddddddddddddd', 1, '2019-02-17 22:31:28', NULL, 'tunis tunis', 2, 1, NULL),
(8, 2, 1, 1, 'dqsdqsdqsd', NULL, NULL, 2, 200, 2, '20181117_000938.jpg', 0, NULL, 'qsdsqdsfsdf', 1, '2019-02-17 22:31:52', NULL, 'sdqsd', 1, 0, NULL),
(9, 2, 1, 1, 'dqsdqsdqsd', NULL, NULL, 1, 200, 2, '20181117_000938.jpg', 0, NULL, 'qsdsqdsfsdf', 1, '2019-02-17 22:32:17', NULL, 'sdqsd', 1, 0, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `regions`
--

CREATE TABLE `regions` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `code` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `regions`
--

INSERT INTO `regions` (`id`, `parent_id`, `code`, `name`) VALUES
(1, NULL, 'GTUN', 'Grand Tunis'),
(2, 1, 'TUN', 'Tunis'),
(3, 1, 'ARN', 'Ariana'),
(4, 1, 'MAN', 'Mannouba'),
(5, 1, 'BAR', 'Ben Arous');

-- --------------------------------------------------------

--
-- Structure de la table `request`
--

CREATE TABLE `request` (
  `id` int(11) NOT NULL,
  `region_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `budget` int(11) DEFAULT NULL,
  `surface_min` int(11) DEFAULT NULL,
  `surface_max` int(11) DEFAULT NULL,
  `pieces` int(11) DEFAULT NULL,
  `type` int(11) NOT NULL,
  `description` longtext NOT NULL,
  `created_at` datetime NOT NULL,
  `equipements` longtext NOT NULL COMMENT '(DC2Type:array)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `phone` varchar(55) NOT NULL,
  `type` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `firstname`, `lastname`, `email`, `phone`, `type`) VALUES
(1, 'hanenimmo', 'HanenImmo2019', 'Hanen', 'Jridi', 'jridi.hanen92@gmail.com', '50770905', 1),
(2, 'wmnasri', 'walid00', 'Walid', 'Mnasri', 'mnasri.walid@gmail.com', '+33767842224', 1),
(9, NULL, NULL, 'mnasri', 'walid', '', '54545', 3),
(10, NULL, NULL, 'Walid', 'MNASRI', 'mnasri.walid@it-prospect.com', '29576148', 3),
(11, NULL, NULL, 'Walid', 'MNASRI', 'mnasri.walid@it-prospect.com', '29576148', 3),
(12, NULL, NULL, 'Walid', 'MNASRI', 'mnasri.walid@it-prospect.com', '29576148', 3),
(13, NULL, NULL, 'Walid', 'MNASRI', 'mnasri.walid@it-prospect.com', '29576148', 3),
(14, NULL, NULL, 'Mnassri', 'Walid', 'mnasriwalid@gmail.com', '59045559', 3),
(15, NULL, NULL, 'Mnassri', 'Walid', 'mnasriwalid@gmail.com', '59045559', 3),
(16, NULL, NULL, 'Walid', 'MNASRI', 'mnasri.walid@it-prospect.com', '29576148', 3);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Index pour la table `newsletter`
--
ALTER TABLE `newsletter`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `pictures`
--
ALTER TABLE `pictures`
  ADD PRIMARY KEY (`id`),
  ADD KEY `property_id` (`property_id`);

--
-- Index pour la table `properties`
--
ALTER TABLE `properties`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`Category_id`),
  ADD KEY `region_id` (`region_id`),
  ADD KEY `IDX_87C331C744BF1B65` (`proposed_by`);

--
-- Index pour la table `regions`
--
ALTER TABLE `regions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent_id` (`parent_id`);

--
-- Index pour la table `request`
--
ALTER TABLE `request`
  ADD PRIMARY KEY (`id`),
  ADD KEY `region_id` (`region_id`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT pour la table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT pour la table `newsletter`
--
ALTER TABLE `newsletter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `pictures`
--
ALTER TABLE `pictures`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT pour la table `properties`
--
ALTER TABLE `properties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT pour la table `regions`
--
ALTER TABLE `regions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `request`
--
ALTER TABLE `request`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `contacts`
--
ALTER TABLE `contacts`
  ADD CONSTRAINT `contacts_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `pictures`
--
ALTER TABLE `pictures`
  ADD CONSTRAINT `FK_8F7C2FC0549213EC` FOREIGN KEY (`property_id`) REFERENCES `properties` (`id`);

--
-- Contraintes pour la table `properties`
--
ALTER TABLE `properties`
  ADD CONSTRAINT `FK_87C331C744BF1B65` FOREIGN KEY (`proposed_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `FK_87C331C798260155` FOREIGN KEY (`region_id`) REFERENCES `regions` (`id`),
  ADD CONSTRAINT `FK_87C331C7C54C8C93` FOREIGN KEY (`Category_id`) REFERENCES `categories` (`id`);

--
-- Contraintes pour la table `regions`
--
ALTER TABLE `regions`
  ADD CONSTRAINT `FK_A26779F3727ACA70` FOREIGN KEY (`parent_id`) REFERENCES `regions` (`id`);

--
-- Contraintes pour la table `request`
--
ALTER TABLE `request`
  ADD CONSTRAINT `request_ibfk_1` FOREIGN KEY (`region_id`) REFERENCES `regions` (`id`),
  ADD CONSTRAINT `request_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`),
  ADD CONSTRAINT `request_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
